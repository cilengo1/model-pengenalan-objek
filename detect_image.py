import colorsys
import os, sys, argparse
import cv2
import time
from timeit import default_timer as timer
import tensorflow as tf
import numpy as np
from tensorflow.keras import backend as K
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.layers import Input, Lambda
from tensorflow_model_optimization.sparsity import keras as sparsity
from PIL import Image
from gtts import gTTS

from yolo3.model import get_yolo3_model, get_yolo3_inference_model
from yolo3.postprocess_np import yolo3_postprocess_np
from yolo2.model import get_yolo2_model, get_yolo2_inference_model
from yolo2.postprocess_np import yolo2_postprocess_np
from common.data_utils import preprocess_image
from common.utils import get_classes, get_anchors, get_colors, optimize_tf_gpu, _draw_boxes3
import operator
from operator import mul
from functools import reduce
from collections import OrderedDict
import matplotlib.pyplot as plt
from tqdm import tqdm
import json

import MNN
import onnxruntime

from common.utils import get_dataset, get_classes, get_anchors, get_colors, get_colors_fixed_map, _draw_boxes3, \
    optimize_tf_gpu, \
    get_custom_objects
import math
from pathlib import Path

def detect_image(image):
    # raise Exception("this function is not supported anymore")
    model_image_size = (256, 256)
    if model_image_size != (None, None):
        assert model_image_size[0]%32 == 0, 'Multiples of 32 required'
        assert model_image_size[1]%32 == 0, 'Multiples of 32 required'
        
    image_data = preprocess_image(image, model_image_size)
    #origin image shape, in (height, width) format
    image_shape = tuple(reversed(image.size))
        
    start = time.time()
    out_boxes, out_classes, out_scores, out_distances = predict(image_data, image_shape)
    print('Found {} boxes for {}'.format(len(out_boxes), 'img'))
    end = time.time()
    print("Inference time: {:.8f}s".format(end - start))

    
    class_color_map = get_colors_fixed_map()
    classes = {0 : 'Pedestrian',
               1 : 'Car',
               2 : 'Van',
               3 : 'Truck',
               4 : 'Person_sitting',
               5 : 'Cyclist',
               6 : 'Tram'
               }
    pred_class = []
    for i in out_classes :
        pred_class.append(classes[i])
    print(pred_class)
    colors = [class_color_map[c] for c in pred_class]
    labels = []
    mytext=''
    for i in range(len(out_boxes)) :
        iclass = str(pred_class[i])  # Mengonversi iclass ke string
        idistance = list(map("{:.0f}".format,out_distances[i]))  # Mengonversi idistance ke string dengan 0 desimal
        text = f"{iclass} {idistance[0]}m"
        if out_distances[i] <=5 :
            mytext = 'Becareful ' + iclass + ' in' + idistance[0] + ' meter'
        labels.append(text)
    #draw result on input image
    image_array = np.array(image, dtype='uint8')
    image_array = _draw_boxes3(image_array, out_boxes, out_classes, colors, drawn_labels=labels)
    # TTS
    language = 'en'
    try :
        myobj = gTTS(text=mytext, lang=language, slow=False)
        myobj.save("result/suara/output.mp3")
        os.system("start result/suara/output.mp3")
        return Image.fromarray(image_array)
    except AssertionError:
        return Image.fromarray(image_array)

def predict(image_data, image_shape):
    custom_object_dict = get_custom_objects()
    anchors = get_anchors('configs/yolo3_anchors.txt')
    model_image_size = (256, 256)
    class_names = get_classes('configs/kitty_all_except_nodata.txt ')
    # load model 
    model_path = 'D:/backup/kuliah/hibah/yolo-with-distance/log/2023_09_18_20_02_29/trained_coba_dumped.h5'
    model = load_model(model_path, compile=False, custom_objects=custom_object_dict)
    model_format = 'H5'
    K.set_learning_phase(0)
    num_anchors = 9
    if num_anchors == 5:
        # YOLOv2 use 5 anchors
        out_boxes, out_classes, out_scores = yolo2_postprocess_np(model.predict(image_data), image_shape, anchors, len(class_names), model_image_size, max_boxes=100, elim_grid_sense=False)
    else:
        out_boxes, out_classes, out_scores, out_distances = yolo3_postprocess_np(model.predict(image_data), image_shape, anchors, len(class_names), model_image_size, max_boxes=100, elim_grid_sense=False)
    return out_boxes, out_classes, out_scores, out_distances

img = input('Input image filename:')
try:
    image = Image.open(img)
except:
    print('Open Error! Try again!')    
else:
    r_image = detect_image(image)
    r_image.show()
        
