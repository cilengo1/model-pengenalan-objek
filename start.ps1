this file is obsolete. use "to_run.md" instead

** Kitty / car-truck /original
* training
python train.py
  --model_type yolo3_xception
  --model_image_size 608x608
  --annotation_file
  "D:\DataSets\kitty\yolo3_labels\all_except_nodata_train.txt"
  --anchors_path configs/yolo3_anchors.txt
  --freeze_level 0
  --classes_path configs/kitty_all_except_nodata.txt
  --total_epoch 200
  --batch_size 3






-- pomocné:

train:
python train.py
  --model_type yolo3_xception
  --model_image_size 608x608
  --annotation_file data-for-yolo-training-all-cut.txt
  --eval_online
  --eval_epoch_interval 1000
  --save_eval_checkpoint
  --anchors_path configs/yolo3_anchors.txt
  --freeze_level 0
  --classes_path configs/custom_class7.txt
  --total_epoch 200
  --batch_size 4

dump modelu po trenovani na inferenci:
python yolo.py --model_type
=yolo3_mobilenet_lite --weights_path=logs/000/ep197-loss7.443-val_loss7.939.h5 --anchors_path configs/yolo3_anchors.txt --classes_path configs/custom_class7.txt --model_image_size=416x416 --dump_model --output_model_file=weights/temp4.h5

inference:
python yolo.py --image --model_type
 yolo3_mobilenet_lite --weights_path weights/temp4.h5 --anchors_path configs/yolo3_anchors.txt --classes_path configs/custom_class7.txt --model_image_size=416x416